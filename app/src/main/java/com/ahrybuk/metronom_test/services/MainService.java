package com.ahrybuk.metronom_test.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.media.AudioManager;
import android.os.IBinder;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.SoundEffectConstants;

import com.ahrybuk.metronom_test.MainActivity;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by hrybuk on 21.03.2016.
 */
public class MainService extends Service {
    private BpmHasChangedReciever bpmHasChangedReciever = new BpmHasChangedReciever();
    private SoundEffectStateReceiver soundEffectStateReceiver = new SoundEffectStateReceiver();
    private FlashEffectStateReceiver flashEffectStateReceiver = new FlashEffectStateReceiver();
    private VibroEffectStateReceiver vibroEffectStateReceiver = new VibroEffectStateReceiver();
    private boolean sound, vibro, flash, camConnect, flashOn;
    private LocalBroadcastManager localBroadcastManager;
    private Timer timer;
    private Camera camera;
    private Parameters parameters;
    private int bpm;

    @Override
    public void onCreate() {
        super.onCreate();
        localBroadcastManager = LocalBroadcastManager.getInstance(this);
        timer = new Timer();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        bpm = intent.getIntExtra("BPM", 100);
        vibro = intent.getBooleanExtra("VIBRO", false);
        flash = intent.getBooleanExtra("FLASH", false);
        sound = intent.getBooleanExtra("SOUND", false);
        camConnect();
        localBroadcastManager.registerReceiver(bpmHasChangedReciever, new IntentFilter(MainActivity.BPM_CHANGED_ACTION));
        localBroadcastManager.registerReceiver(soundEffectStateReceiver, new IntentFilter(MainActivity.SOUND_EFFECT_CHOOSED_ACTION));
        localBroadcastManager.registerReceiver(flashEffectStateReceiver, new IntentFilter(MainActivity.FLASH_EFFECT_CHOOSED_ACTION));
        localBroadcastManager.registerReceiver(vibroEffectStateReceiver, new IntentFilter(MainActivity.VIBRO_EFFECT_CHOOSED_ACTION));
        startMetronome(getPeriod(bpm));
        Log.d("MainActivity", "bpm is " + bpm);

        return START_REDELIVER_INTENT;
    }

    private long getPeriod(int bpm) {
        double temp = (double) 60 / bpm * 1000;
        return (long) temp;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        timer.cancel();
        camDisconect();
        localBroadcastManager.unregisterReceiver(bpmHasChangedReciever);
        localBroadcastManager.unregisterReceiver(soundEffectStateReceiver);
        localBroadcastManager.unregisterReceiver(flashEffectStateReceiver);
        localBroadcastManager.unregisterReceiver(vibroEffectStateReceiver);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private class BpmHasChangedReciever extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("MainActivity", "BPM HAS CHANGED");
            bpm = intent.getIntExtra("BPM", 100);
            timer.cancel();
            startMetronome(getPeriod(bpm));
        }
    }

    private class SoundEffectStateReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            sound = !sound;
        }
    }

    private class FlashEffectStateReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            flash = !flash;
            if (flashOn) {
                parameters.setFlashMode(Parameters.FLASH_MODE_OFF);
                camera.setParameters(parameters);
                flashOn = false;
            }
        }
    }

    private class VibroEffectStateReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            vibro = !vibro;
        }
    }

    private void camConnect() {
        if (camera != null) {
            camDisconect();
        }
            /* Camera initialize */
        if (camera == null) {
            camera = Camera.open();
            parameters = camera.getParameters();
            try {
                camera.setPreviewTexture(new SurfaceTexture(0));
            } catch (IOException e) {
                e.printStackTrace();
            }
            camera.startPreview();
            camConnect = true;
        }
    }

    private void camDisconect() {
        if (camera != null) {
            parameters.setFlashMode(Parameters.FLASH_MODE_OFF);
            camera.setParameters(parameters);
            camera.release();
            camera = null;
            camConnect = false;
        }
    }

    //
    private void startMetronome(final long bpm) {
        final TimerTask metronomeTask = new TimerTask() {
            AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

            @Override
            public void run() {
                {
                    localBroadcastManager.sendBroadcast(new Intent(MainActivity.CHANGE_INDICATOR_STATE_ACTION));
                    if (sound) {
                        audioManager.playSoundEffect(SoundEffectConstants.NAVIGATION_DOWN);
                    }
                    if (vibro) {
                        vibrator.vibrate(50);
                    }
                    if (flash && camConnect) {
                        if (flashOn) {
                            parameters.setFlashMode(Parameters.FLASH_MODE_OFF);
                            camera.setParameters(parameters);
                            flashOn = false;

                        } else {
                            parameters.setFlashMode(Parameters.FLASH_MODE_TORCH);
                            camera.setParameters(parameters);
                            flashOn = true;
                        }
                    }
                }
            }
        };
        timer = new Timer();
        timer.scheduleAtFixedRate(metronomeTask, bpm, bpm);
    }
}

