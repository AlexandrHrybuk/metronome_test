package com.ahrybuk.metronom_test;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ahrybuk.metronom_test.services.MainService;

/**
 * Created by hrybuk on 21.03.2016.
 */
public class MainActivity extends Activity implements SeekBar.OnSeekBarChangeListener, View.OnClickListener, TextWatcher {
    private final String LOG_TAG = MainActivity.class.getSimpleName();
    //Intent actions
    public static final String CHANGE_INDICATOR_STATE_ACTION = "CHANGE_INDICATOR_STATE_ACTION";
    public static final String BPM_CHANGED_ACTION = "BPM_CHANGED_ACTION";
    public static final String SOUND_EFFECT_CHOOSED_ACTION = "SOUND_EFFECT_CHOOSED_ACTION";
    public static final String FLASH_EFFECT_CHOOSED_ACTION = "FLASH_EFFECT_CHOOSED_ACTION";
    public static final String VIBRO_EFFECT_CHOOSED_ACTION = "VIBRO_EFFECT_CHOOSED_ACTION";

    private final int MAX_BPM = 210;
    private final int DEFAULT_BPM = 105;
    private final int SHIFT = 50;

    private ChangeIndicatorStateBroadcastReceiver changeIndicatorStateBroadcastReceiver = new ChangeIndicatorStateBroadcastReceiver();
    private LocalBroadcastManager localBroadcastManager;
    private SeekBar seekBar;
    private TextView currentBpm;
    private EditText bpmEditText;
    private Button startStopBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity_layout);

        localBroadcastManager = LocalBroadcastManager.getInstance(this);

        findViewById(R.id.vibro_btn).setOnClickListener(this);
        findViewById(R.id.flash_btn).setOnClickListener(this);
        findViewById(R.id.sound_btn).setOnClickListener(this);
        findViewById(R.id.bpm_minus).setOnClickListener(this);
        findViewById(R.id.bpm_plus).setOnClickListener(this);
        findViewById(R.id.current_bpm).setOnClickListener(this);
        findViewById(R.id.start_stop_btn).setOnClickListener(this);

        initializeViewElements();
    }

    @Override
    protected void onResume() {
        super.onResume();
        localBroadcastManager.registerReceiver(changeIndicatorStateBroadcastReceiver, new IntentFilter(CHANGE_INDICATOR_STATE_ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        localBroadcastManager.unregisterReceiver(changeIndicatorStateBroadcastReceiver);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        bpmEditText.getText().clear();
        bpmEditText.setText(String.valueOf(progress + SHIFT));
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }
            /* Initializers */

    private void initializeViewElements() {


        seekBar = (SeekBar) findViewById(R.id.seek_bar);
        seekBar.setMax(MAX_BPM);
        seekBar.setProgress(DEFAULT_BPM);
        seekBar.setOnSeekBarChangeListener(this);

        currentBpm = (TextView) findViewById(R.id.current_bpm);
        currentBpm.setText(Integer.valueOf(DEFAULT_BPM + SHIFT).toString());

        bpmEditText = (EditText) findViewById(R.id.set_bpm);
        bpmEditText.setText(Integer.valueOf(DEFAULT_BPM + SHIFT).toString());
        bpmEditText.addTextChangedListener(this);

        startStopBtn = (Button) findViewById(R.id.start_stop_btn);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.vibro_btn:
                localBroadcastManager.sendBroadcast(new Intent(VIBRO_EFFECT_CHOOSED_ACTION));
                (findViewById(R.id.vibro_btn)).setSelected(!(findViewById(R.id.vibro_btn)).isSelected());
                break;
            case R.id.flash_btn:
                localBroadcastManager.sendBroadcast(new Intent(FLASH_EFFECT_CHOOSED_ACTION));
                (findViewById(R.id.flash_btn)).setSelected(!(findViewById(R.id.flash_btn)).isSelected());
                break;
            case R.id.sound_btn:
                localBroadcastManager.sendBroadcast(new Intent(SOUND_EFFECT_CHOOSED_ACTION));
                (findViewById(R.id.sound_btn)).setSelected(!(findViewById(R.id.sound_btn)).isSelected());
                break;
            case R.id.bpm_plus:
                int currentBpm = Integer.valueOf(String.valueOf(bpmEditText.getText().toString()));
                bpmEditText.getText().clear();
                bpmEditText.setText(String.valueOf(currentBpm + 2));
                this.currentBpm.setText(String.valueOf(currentBpm + 2 - SHIFT));
                break;
            case R.id.bpm_minus:
                int _currentBpm = Integer.valueOf(String.valueOf(bpmEditText.getText().toString()));
                bpmEditText.getText().clear();
                bpmEditText.setText(String.valueOf(_currentBpm - 2));
                this.currentBpm.setText(String.valueOf(_currentBpm - 2 - SHIFT));
                break;
            case R.id.current_bpm:
                bpmEditText.requestFocus();
                bpmEditText.setSelection(bpmEditText.getText().length());
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(bpmEditText, InputMethodManager.SHOW_IMPLICIT);
                break;
            case R.id.start_stop_btn:
                Intent intent = new Intent(this, MainService.class);
                intent.putExtra("BPM", Integer.valueOf(String.valueOf(this.currentBpm.getText().toString())));
                intent.putExtra("VIBRO", (findViewById(R.id.vibro_btn)).isSelected());
                intent.putExtra("FLASH", (findViewById(R.id.flash_btn)).isSelected());
                intent.putExtra("SOUND", (findViewById(R.id.sound_btn)).isSelected());
                if (!isMyServiceRunning(MainService.class)) {
                    if(validateCurrentBpm(Integer.valueOf(bpmEditText.getText().toString()))){
                        startService(intent);
                        startStopBtn.setText(getResources().getString(R.string.stop));
                    }
                } else {
                    stopService(intent);
                    startStopBtn.setText(getResources().getString(R.string.start));
                }
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (bpmEditText.getText().toString().equals("")) {
            bpmEditText.setText("0");
            bpmEditText.setSelection(1);
            validateNewBpm(0);
        }else{
            validateNewBpm(Integer.valueOf(String.valueOf(s)));
        }
        seekBar.setProgress(Integer.valueOf(String.valueOf(s)));
    }

    private boolean validateCurrentBpm(int bpm){
        if ((bpm > MAX_BPM + SHIFT) || (bpm < SHIFT)) {
            Toast.makeText(this, "BPM must be from 50 to 260", Toast.LENGTH_SHORT).show();
            return false;
        }else return true;
    }

    private void validateNewBpm(int bpm) {
        currentBpm.setText(String.valueOf(Integer.valueOf(bpmEditText.getText().toString())));
        if ((bpm > MAX_BPM + SHIFT) || (bpm < SHIFT)) {
            Toast.makeText(this, "BPM must be from 50 to 260", Toast.LENGTH_SHORT).show();
        }else{
            Intent bpmChangedIntent = new Intent(BPM_CHANGED_ACTION);
            bpmChangedIntent.putExtra("BPM", bpm);
            localBroadcastManager.sendBroadcast(bpmChangedIntent);
        }
    }

    private void setNewBpm(int bpm) {
        if (bpm > MAX_BPM + SHIFT) {
            currentBpm.setText(String.valueOf(MAX_BPM + SHIFT));
        }
        if(bpm < SHIFT){
            currentBpm.setText(String.valueOf(SHIFT));
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private class ChangeIndicatorStateBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            (findViewById(R.id.indicator)).setSelected(!findViewById(R.id.indicator).isSelected());
        }
    }
}
